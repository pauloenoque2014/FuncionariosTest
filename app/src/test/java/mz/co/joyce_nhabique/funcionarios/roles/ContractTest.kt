package mz.co.joyce_nhabique.funcionarios.roles

import mz.co.joyce_nhabique.funcionarios.model.Employee
import mz.co.joyce_nhabique.funcionarios.model.enums.Roles
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Created by Paulo Enoque on 11/6/2017.
 */
class ContractTest{
    lateinit var employee : Employee

    @Before
    fun init(){
        employee = Employee("Paulo")
    }

    @Test
    fun developerWithTeenYears(){
        val salary = employee.reole.contract.salary(10)
        Assert.assertEquals(8500.0,salary,0.000001)
    }

    @Test
    fun testerWithTeenYears(){
        employee.reole = Roles.TESTER
        val salary = employee.reole.contract.salary(10)
        Assert.assertEquals(8000.0,salary,0.000001)
    }

    @Test
    fun analyticTeenYears(){
        employee.reole = Roles.ANALYST
        val salary = employee.reole.contract.salary(10)
        Assert.assertEquals(9000.0,salary,0.000001)
    }
}