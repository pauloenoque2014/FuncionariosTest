package mz.co.joyce_nhabique.funcionarios.viewModel.businessLogin

import mz.co.joyce_nhabique.funcionarios.model.interfaces.Contract

/**
 * Created by Paulo Enoque on 11/6/2017.
 */
class TeenPercentContract : Contract{

    override fun salary(expirience: Int) =
            if (expirience > 0) 1000 * expirience * 0.9 else 1000 * 1 * 0.9

}