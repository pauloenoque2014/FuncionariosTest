package mz.co.joyce_nhabique.funcionarios.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import mz.co.joyce_nhabique.funcionarios.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
