package mz.co.joyce_nhabique.funcionarios.model.enums

import mz.co.joyce_nhabique.funcionarios.model.interfaces.Contract
import mz.co.joyce_nhabique.funcionarios.viewModel.businessLogin.FifteenPercentContract
import mz.co.joyce_nhabique.funcionarios.viewModel.businessLogin.TeenPercentContract
import mz.co.joyce_nhabique.funcionarios.viewModel.businessLogin.TwentyPercentContract

/**
 * Created by Paulo Enoque on 11/6/2017.
 */
enum class Roles(val contract: Contract){
    DEVELOPERS(FifteenPercentContract()), TESTER(TwentyPercentContract()), ANALYST(TeenPercentContract());

}