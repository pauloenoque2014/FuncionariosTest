package mz.co.joyce_nhabique.funcionarios.model

import mz.co.joyce_nhabique.funcionarios.model.enums.Roles

/**
 * Created by Paulo Enoque on 11/6/2017.
 */
data class Employee(var name: String, var reole: Roles = Roles.DEVELOPERS)